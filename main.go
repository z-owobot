package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"github.com/mnlwldr/owo"
)

var on = make(map[string]bool)
var (
	token        string
	webhookid    string
	webhooktoken string
)

func main() {
	godotenv.Load(".env")

	token = os.Getenv("TOKEN")
	webhookid = os.Getenv("WEBHOOK_ID")
	webhooktoken = os.Getenv("WEBHOOK_TOKEN")

	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("error with bot making", err)
	}
	dg.AddHandler(messageCreate)

	dg.Identify.Intents = discordgo.IntentsAllWithoutPrivileged

	err = dg.Open()

	if err != nil {
		fmt.Println("oopsie happen on Open()", err)
		return
	}

	fmt.Println("bot running ctrl-c to die")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	dg.Close()

}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if m.Content == "owo on" {
		on[m.Author.ID] = true
		s.ChannelMessageSend(m.ChannelID, "on")

	} else if m.Content == "owo off" {
		on[m.Author.ID] = false
		s.ChannelMessageSend(m.ChannelID, "off")
	} else {
		if on[m.Author.ID] {
			s.ChannelMessageDelete(m.ChannelID, m.Message.ID)
			toSend := owo.Translate(m.Content, false, true)

			s.WebhookExecute(webhookid, webhooktoken, false,
				&discordgo.WebhookParams{
					Content: toSend, Username: m.Member.Nick, AvatarURL: fmt.Sprint("https://cdn.discordapp.com/", m.Author.ID, "/", m.Author.Avatar),
					AllowedMentions: &discordgo.MessageAllowedMentions{},
				})

		}
	}
}
