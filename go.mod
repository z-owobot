module git.sr.ht/~zeromomentum/owobot

go 1.15

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/joho/godotenv v1.4.0
	github.com/mnlwldr/owo v0.0.0-20200904135418-3b7a093d94a8
)
